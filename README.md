# devops-netology
Домашнее задание по лекции "Системы контроля версий"

В папке terraform будут игнорироваться:
1) всё содержимое папки .terraform;
2) файлы .tfstate, .tfstate.*;
3) файлы .crash.log, .crash.*.log;
4) файлы .tfvars
5) файлы override.tf, override.tf.json, *_override.tf, *_override.tf.json;
6) файлы .terraformrc, terraform.rc.